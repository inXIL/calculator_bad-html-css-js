var output = document.getElementById("output");
if (!(output instanceof HTMLInputElement)) {
    throw new Error("Expected output to be HTMLInputElement");
}
var _loop_1 = function (i) {
    document.getElementById(i.toString()).addEventListener("click", function () {
        output.value += i.toString();
    });
};
for (var i = 0; i < 10; ++i) {
    _loop_1(i);
}
var arg = "0";
var operand = "";
document.querySelectorAll(".op").forEach(function (op) { return op.addEventListener("click", function () {
    arg = output.value;
    operand = op.id;
    output.value = "";
}); });
document.getElementById("C").addEventListener("click", function () {
    output.value = "";
    operand = "";
    arg = "";
});
document.getElementById("=").addEventListener("click", function () {
    var arg1 = parseFloat(arg);
    var arg2 = parseFloat(output.value);
    var out = parseFloat(output.value);
    switch (operand) {
        case "+":
            out = arg1 + arg2;
            break;
        case "-":
            out = arg1 - arg2;
            break;
        case "x":
            out = arg1 * arg2;
            break;
        case "/":
            out = arg1 / arg2;
            break;
    }
    output.value = out.toString();
    operand = "";
    arg = "";
});
