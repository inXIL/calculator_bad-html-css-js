const output = document.getElementById("output")
if (!(output instanceof HTMLInputElement)) {
    throw new Error(`Expected output to be HTMLInputElement`)
}
for(let i = 0; i < 10; ++i) {
    document.getElementById(i.toString()).addEventListener("click", () => {
        output.value += i.toString()
    })
}
let arg = "0"
let operand = ""
document.querySelectorAll(".op").forEach(
    op => op.addEventListener("click", () => {
        arg = output.value
        operand = op.id
        output.value = ""
    })
)
document.getElementById("C").addEventListener("click", () => {
    output.value = ""
    operand = ""
    arg = ""
})
document.getElementById("=").addEventListener("click", () => {
    const arg1 = parseFloat(arg)
    const arg2 = parseFloat(output.value)
    let out = parseFloat(output.value)
    switch(operand) {
        case "+": out = arg1 + arg2; break;
        case "-": out = arg1 - arg2; break;
        case "x": out = arg1 * arg2; break;
        case "/": out = arg1 / arg2; break;
    }
    output.value = out.toString()
    operand = ""
    arg = ""
})